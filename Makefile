PROJECT = ice40_rs485

SRC = $(foreach dir, ./, $(wildcard $(dir)/*.v))
TOP = top

BUILD = build
BLIF = $(BUILD)/$(PROJECT).blif
PCF  = pinout.pcf
ASC  = $(BUILD)/$(PROJECT).asc
BIN  = $(BUILD)/$(PROJECT).bin

all: $(BIN) time

$(BLIF): $(SRC) $(BUILD)/icarus.log
	yosys -p "synth_ice40 -blif $@ -top $(TOP)" $(SRC) | tee $(BUILD)/yosys.log

$(ASC): $(BLIF) $(PCF)
	arachne-pnr -r -d 8k -p $(PCF) $(BLIF) -o $@ | tee $(BUILD)/arachne-pnr.log

$(BIN): $(ASC)
	icepack $^ $@ | tee $(BUILD)/icepack.log

program: $(BIN)
	iceprog $^

$(BUILD)/icarus.log: $(SRC)
	mkdir -p $(BUILD)
	iverilog $^ -s $(TOP) -tnull | tee $@

$(BUILD)/time.log: $(ASC)
	icetime -d hx8k -c 12 $(ASC) | tee $@

icarus: $(BUILD)/icarus.log
time: $(BUILD)/time.log

clean:
	rm -rf $(BUILD)

.PHONY: all program icarus time clean

