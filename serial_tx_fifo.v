module serial_tx_fifo(
	clock, reset,
	baud, tx,
	byte, byte_valid,
	active, start_byte, end_byte,
	fifo_full, fifo_error);

input wire clock;
input wire reset;

input  wire [15:0] baud;
output wire        tx;

input wire [7:0] byte;
input wire       byte_valid;

output wire active;

output wire start_byte;
output wire end_byte;

output wire fifo_full;
output wire fifo_error;

wire       fifo_empty;
wire [7:0] fifo_rd_data;
byte_fifo tx_fifo(
	.clock   (clock),
	.reset   (reset),
	.wr_en   (byte_valid),
	.wr_data (byte),
	.rd_en   (start_byte),
	.rd_data (fifo_rd_data),
	.empty   (fifo_empty),
	.full    (fifo_full),
	.error   (fifo_error));

serial_tx dev_tx(
	.clock      (clock),
	.reset      (reset),
	.baud       (baud),
	.tx         (tx),
	.byte       (fifo_rd_data),
	.byte_valid (!fifo_empty),
	.active     (active),
	.start_byte (start_byte),
	.end_byte   (end_byte));

endmodule
