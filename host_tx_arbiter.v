module host_tx_arbiter(
	clock, reset,
	tx_a_ts_full, tx_a_ts_ready, tx_a_ts_data, tx_a_ts_read,
	tx_b_ts_full, tx_b_ts_ready, tx_b_ts_data, tx_b_ts_read,
	rx_a_full, rx_a_ready, rx_a_data, rx_a_read,
	rx_b_full, rx_b_ready, rx_b_data, rx_b_read,
	cmd_full, cmd_ready, cmd_data, cmd_read,
	error_ready, error_data, error_read,
	out_read, out_valid, out_data);

input wire clock;
input wire reset;

input wire       tx_a_ts_full;
input wire       tx_a_ts_ready;
input wire [7:0] tx_a_ts_data;
output reg       tx_a_ts_read;

input wire       tx_b_ts_full;
input wire       tx_b_ts_ready;
input wire [7:0] tx_b_ts_data;
output reg       tx_b_ts_read;

input wire       rx_a_full;
input wire       rx_a_ready;
input wire [7:0] rx_a_data;
output reg       rx_a_read;

input wire       rx_b_full;
input wire       rx_b_ready;
input wire [7:0] rx_b_data;
output reg       rx_b_read;

input wire       cmd_full;
input wire       cmd_ready;
input wire [7:0] cmd_data;
output reg       cmd_read;

input wire       error_ready;
input wire [7:0] error_data;
output reg       error_read;

input wire       out_read;
output reg       out_valid;
output reg [7:0] out_data;


`define DEV_CMD_RECV_DATA_A    2
`define DEV_CMD_RECV_DATA_B    3
`define DEV_CMD_TIMESTAMP_TX_A 4
`define DEV_CMD_TIMESTAMP_TX_B 5
`define DEV_CMD_ERROR          8'hFF

`define MODE_NONE    0
`define MODE_TX_A_TS 1
`define MODE_TX_B_TS 2
`define MODE_RX_A    3
`define MODE_RX_B    4
`define MODE_CMD     5
`define MODE_ERROR   6

reg [2:0] mode;
reg [3:0] count;

initial
begin
	tx_a_ts_read <= 0;
	tx_b_ts_read <= 0;
	rx_a_read    <= 0;
	rx_b_read    <= 0;
	cmd_read     <= 0;
	error_read   <= 0;
	out_valid    <= 0;

	mode <= `MODE_NONE;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		tx_a_ts_read <= 0;
		tx_b_ts_read <= 0;
		rx_a_read    <= 0;
		rx_b_read    <= 0;
		cmd_read     <= 0;
		error_read   <= 0;
		out_valid    <= 0;

		mode <= `MODE_NONE;
	end
	else if (!out_valid | out_read)
	begin
		case (mode)
			`MODE_NONE:
			begin
				if (error_ready)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_ERROR;
					mode      <= `MODE_ERROR;
					count     <= 3;
					cmd_read  <= 0;
				end
				else if (tx_a_ts_full)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_TIMESTAMP_TX_A;
					mode      <= `MODE_TX_A_TS;
					count     <= 7;
					cmd_read  <= 0;
				end
				else if (tx_b_ts_full)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_TIMESTAMP_TX_B;
					mode      <= `MODE_TX_B_TS;
					count     <= 7;
					cmd_read  <= 0;
				end
				else if (rx_a_full)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_RECV_DATA_A;
					mode      <= `MODE_RX_A;
					count     <= 8;
					cmd_read  <= 0;
				end
				else if (rx_b_full)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_RECV_DATA_B;
					mode      <= `MODE_RX_B;
					count     <= 8;
					cmd_read  <= 0;
				end
				else if (cmd_full)
				begin
					out_valid <= 1;
					out_data  <= cmd_data;
					mode      <= `MODE_CMD;
					count     <= 3;
					cmd_read  <= 1;
				end
				else if (tx_a_ts_ready)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_TIMESTAMP_TX_A;
					mode      <= `MODE_TX_A_TS;
					count     <= 7;
					cmd_read  <= 0;
				end
				else if (tx_b_ts_ready)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_TIMESTAMP_TX_B;
					mode      <= `MODE_TX_B_TS;
					count     <= 7;
					cmd_read  <= 0;
				end
				else if (rx_a_ready)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_RECV_DATA_A;
					mode      <= `MODE_RX_A;
					count     <= 8;
					cmd_read  <= 0;
				end
				else if (rx_b_ready)
				begin
					out_valid <= 1;
					out_data  <= `DEV_CMD_RECV_DATA_B;
					mode      <= `MODE_RX_B;
					count     <= 8;
					cmd_read  <= 0;
				end
				else if (cmd_ready)
				begin
					out_valid <= 1;
					out_data  <= cmd_data;
					mode      <= `MODE_CMD;
					count     <= 3;
					cmd_read  <= 1;
				end
				else
				begin
					out_valid <= 0;
					cmd_read  <= 0;
				end

				tx_a_ts_read <= 0;
				tx_b_ts_read <= 0;
				rx_a_read    <= 0;
				rx_b_read    <= 0;
				error_read   <= 0;
			end

			`MODE_TX_A_TS:
			begin
				if (tx_a_ts_ready)
				begin
					count <= (count - 1);
					if (count == 0) mode <= `MODE_NONE;
				end

				out_data     <= tx_a_ts_data;
				out_valid    <= tx_a_ts_ready;
				tx_a_ts_read <= tx_a_ts_ready;
			end

			`MODE_TX_B_TS:
			begin
				if (tx_b_ts_ready)
				begin
					count <= (count - 1);
					if (count == 0) mode <= `MODE_NONE;
				end

				out_data     <= tx_b_ts_data;
				out_valid    <= tx_b_ts_ready;
				tx_b_ts_read <= tx_b_ts_ready;
			end

			`MODE_RX_A:
			begin
				if (rx_a_ready)
				begin
					count <= (count - 1);
					if (count == 0) mode <= `MODE_NONE;
				end

				out_data  <= rx_a_data;
				out_valid <= rx_a_ready;
				rx_a_read <= rx_a_ready;
			end

			`MODE_RX_B:
			begin
				if (rx_b_ready)
				begin
					count <= (count - 1);
					if (count == 0) mode <= `MODE_NONE;
				end

				out_data  <= rx_b_data;
				out_valid <= rx_b_ready;
				rx_b_read <= rx_b_ready;
			end

			`MODE_CMD:
			begin
				if (cmd_ready)
				begin
					count <= (count - 1);
					if (count == 0) mode <= `MODE_NONE;
				end

				out_data  <= cmd_data;
				out_valid <= cmd_ready;
				cmd_read  <= cmd_ready;
			end

			`MODE_ERROR:
			begin
				if (error_ready)
				begin
					count <= (count - 1);
					if (count == 0) mode <= `MODE_NONE;
				end

				out_data   <= error_data;
				out_valid  <= error_ready;
				error_read <= error_ready;
			end
		endcase
	end
	else
	begin
		tx_a_ts_read <= 0;
		tx_b_ts_read <= 0;
		rx_a_read    <= 0;
		rx_b_read    <= 0;
		error_read   <= 0;
		cmd_read     <= 0;
	end
end

endmodule
