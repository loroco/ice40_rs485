module cmd_serialize(
	clock, reset,
	cmd_id, cmd_data, cmd_ready,
	byte, byte_ready, error);

input wire clock;
input wire reset;

input wire [ 7:0] cmd_id;
input wire [31:0] cmd_data;
input wire        cmd_ready;

output reg [7:0] byte;
output reg       byte_ready;
output reg       error;


reg [31:0] fifo;
reg [ 2:0] count;

initial
begin
	byte_ready <= 0;
	count      <= 0;
	error      <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		byte_ready <= 0;
		count      <= 0;
		error      <= 0;
	end
	else
	begin
		if (count > 0)
		begin
			byte       <= fifo[7:0];
			byte_ready <= 1;
			fifo[23:0] <= fifo[31:8];
			count      <= count - 1;
			if (cmd_ready) error <= 1;
		end
		else if (cmd_ready)
		begin
			byte       <= cmd_id;
			byte_ready <= 1;
			fifo[31:0] <= cmd_data;
			count      <= 4;
		end
		else
		begin
			byte_ready <= 0;
		end
	end
end

endmodule
