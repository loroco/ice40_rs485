module byte_fifo(
	clock, reset,
	wr_en, wr_data,
	rd_en, rd_data,
	empty, full, error);

input  wire       clock;
input  wire       reset;

input  wire       wr_en;
input  wire [7:0] wr_data;

input  wire       rd_en;
output wire [7:0] rd_data;

output reg        empty;
output reg        full;
output reg        error;


reg [7:0] fifo[0:255];
reg [7:0] fifo_rd_ptr;
reg [7:0] fifo_wr_ptr;

wire [7:0] fifo_rd_ptr_next = (fifo_rd_ptr + 1);
wire [7:0] fifo_wr_ptr_next = (fifo_wr_ptr + 1);

assign rd_data = fifo[fifo_rd_ptr];

initial
begin
	empty       <= 1;
	full        <= 0;
	error       <= 0;
	fifo_rd_ptr <= 0;
	fifo_wr_ptr <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		empty       <= 1;
		full        <= 0;
		error       <= 0;
		fifo_rd_ptr <= 0;
		fifo_wr_ptr <= 0;
	end
	else
	begin
		if ((full && wr_en && !rd_en)
			|| (empty && rd_en))
			error <= 1;

		if (wr_en)
		begin
			fifo[fifo_wr_ptr] <= wr_data;
			fifo_wr_ptr <= fifo_wr_ptr_next;
		end

		if (rd_en)
			fifo_rd_ptr <= fifo_rd_ptr_next;

		if (wr_en && !rd_en)
		begin
			empty <= 0;
			full  <= (fifo_wr_ptr_next == fifo_rd_ptr);
		end
		else if (rd_en && !wr_en)
		begin
			empty <= (fifo_rd_ptr_next == fifo_wr_ptr);
			full  <= 0;
		end
	end
end

endmodule
