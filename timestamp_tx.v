module timestamp_tx(
	clock, reset,
	timer,
	tx_start, tx_end,
	fifo_wr, fifo_data,
	error);

input wire clock;
input wire reset;

input wire [31:0] timer;

input wire tx_start;
input wire tx_end;

output reg       fifo_wr;
output reg [7:0] fifo_data;

output reg error;


reg [ 2:0] mode;
reg [31:0] ts_start;
reg        ts_start_set;
reg [31:0] ts_end;
reg        ts_end_set;

initial
begin
	fifo_wr      <= 0;
	error        <= 0;
	mode         <= 0;
	ts_start_set <= 0;
	ts_end_set   <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		fifo_wr      <= 0;
		error        <= 0;
		mode         <= 0;
		ts_start_set <= 0;
		ts_end_set   <= 0;
	end
	else
	begin
		if ((mode[2] == 0) && ts_start_set)
		begin
			fifo_wr   <= 1;
			fifo_data <= ts_start[7:0];
			mode      <= mode + 1;

			if (mode[1:0] == 3)
			begin
				if (tx_start) ts_start <= timer;
				ts_start_set <= tx_start;
			end
			else
			begin
				ts_start[23:0] <= ts_start[31:8];
			end

			if (tx_end)
			begin
				ts_end     <= timer;
				ts_end_set <= 1;
			end

			error <= ((tx_start & (mode[1:0] != 3))
				| (tx_end & ts_end_set));
		end
		else if ((mode[2] == 1) && ts_end_set)
		begin
			fifo_wr   <= 1;
			fifo_data <= ts_end[7:0];
			mode      <= mode + 1;

			if (tx_start)
			begin
				ts_start     <= timer;
				ts_start_set <= 1;
			end

			if (mode[1:0] == 3)
			begin
				if (tx_end) ts_end <= timer;
				ts_end_set <= tx_end;
			end
			else
			begin
				ts_end[23:0] <= ts_end[31:8];
			end

			error <= ((tx_start & ts_start_set)
				| (tx_end & (mode[1:0] != 3)));
		end
		else
		begin
			fifo_wr <= 0;

			if (tx_start)
			begin
				ts_start     <= timer;
				ts_start_set <= 1;
			end

			if (tx_end)
			begin
				ts_end     <= timer;
				ts_end_set <= 1;
			end

			error <= ((tx_start & ts_start_set)
				| (tx_end & ts_end_set));
		end
	end
end

endmodule
