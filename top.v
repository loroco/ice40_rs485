module top(
	clock,
	rs232_rx, rs232_tx, rs232_rts_n,
	rs485_a_rx, rs485_a_tx, rs485_a_de, rs485_a_re,
	rs485_b_rx, rs485_b_tx, rs485_b_de, rs485_b_re,
	led);

input wire clock;

input  wire rs232_rx;
output wire rs232_tx;
output wire rs232_rts_n;

input  wire rs485_a_rx;
output wire rs485_a_tx;
output wire rs485_a_de;
output wire rs485_a_re;

input  wire rs485_b_rx;
output wire rs485_b_tx;
output wire rs485_b_de;
output wire rs485_b_re;

output wire [4:0] led;


wire        host_rx_reset;
wire [15:0] host_rx_baud;
wire [15:0] host_rx_fract;
wire [ 7:0] host_rx_byte;
wire        host_rx_byte_valid;
wire        host_rx_stop_error;
serial_rx host_rx(
	.clock      (clock),
	.reset      (host_rx_reset),
	.baud       (host_rx_baud),
	.fract      (host_rx_fract),
	.rx         (rs232_rx),
	.byte       (host_rx_byte),
	.byte_valid (host_rx_byte_valid),
	.stop_error (host_rx_stop_error),
	.start_byte (),
	.end_byte   ());

wire        host_tx_reset;
wire [15:0] host_tx_baud;
wire [ 7:0] host_tx_byte;
wire        host_tx_byte_valid;
wire        host_tx_start_byte;
serial_tx host_tx(
	.clock      (clock),
	.reset      (host_tx_reset),
	.baud       (host_tx_baud),
	.tx         (rs232_tx),
	.byte       (host_tx_byte),
	.byte_valid (host_tx_byte_valid),
	.active     (),
	.start_byte (host_tx_start_byte),
	.end_byte   ());

wire        reset;
wire        cmd_proc_out_data_a_valid;
wire [ 7:0] cmd_proc_out_data_a;
wire        cmd_proc_out_data_b_valid;
wire [ 7:0] cmd_proc_out_data_b;
wire [ 7:0] cmd_proc_cmd_fifo_data;
wire	    cmd_proc_cmd_fifo_ready;
wire	    cmd_proc_cmd_fifo_full;
wire	    cmd_proc_cmd_fifo_read;
wire        cmd_proc_cmd_fifo_error;
wire        cmd_proc_cmd_serialize_error;
wire        cmd_proc_error_enable;
wire        cmd_proc_dev_tx_a_reset;
wire [15:0] cmd_proc_dev_tx_a_baud_div;
wire        cmd_proc_dev_rx_a_reset;
wire        cmd_proc_dev_rx_a_enable;
wire [15:0] cmd_proc_dev_rx_a_baud_div;
wire [15:0] cmd_proc_dev_rx_a_baud_fract;
wire        cmd_proc_dev_tx_b_reset;
wire [15:0] cmd_proc_dev_tx_b_baud_div;
wire        cmd_proc_dev_rx_b_reset;
wire        cmd_proc_dev_rx_b_enable;
wire [15:0] cmd_proc_dev_rx_b_baud_div;
wire [15:0] cmd_proc_dev_rx_b_baud_fract;
wire        cmd_proc_host_rx_reset;
wire [15:0] cmd_proc_host_rx_baud_div;
wire [15:0] cmd_proc_host_rx_baud_fract;
wire        cmd_proc_reset;
cmd_proc_fifo cmd_proc(
	.clock               (clock),
	.reset               (reset),
	.in_data_valid       (host_rx_byte_valid),
	.in_data             (host_rx_byte),
	.out_data_a_valid    (cmd_proc_out_data_a_valid),
	.out_data_a          (cmd_proc_out_data_a),
	.out_data_b_valid    (cmd_proc_out_data_b_valid),
	.out_data_b          (cmd_proc_out_data_b),
	.cmd_fifo_data       (cmd_proc_cmd_fifo_data),
	.cmd_fifo_ready      (cmd_proc_cmd_fifo_ready),
	.cmd_fifo_full       (cmd_proc_cmd_fifo_full),
	.cmd_fifo_read       (cmd_proc_cmd_fifo_read),
	.cmd_fifo_error      (cmd_proc_cmd_fifo_error),
	.cmd_serialize_error (cmd_proc_cmd_serialize_error),
	.error_enable        (cmd_proc_error_enable),
	.dev_tx_a_reset      (cmd_proc_dev_tx_a_reset),
	.dev_tx_a_baud_div   (cmd_proc_dev_tx_a_baud_div),
	.dev_rx_a_reset      (cmd_proc_dev_rx_a_reset),
	.dev_rx_a_enable     (cmd_proc_dev_rx_a_enable),
	.dev_rx_a_baud_div   (cmd_proc_dev_rx_a_baud_div),
	.dev_rx_a_baud_fract (cmd_proc_dev_rx_a_baud_fract),
	.dev_tx_b_reset      (cmd_proc_dev_tx_b_reset),
	.dev_tx_b_baud_div   (cmd_proc_dev_tx_b_baud_div),
	.dev_rx_b_reset      (cmd_proc_dev_rx_b_reset),
	.dev_rx_b_enable     (cmd_proc_dev_rx_b_enable),
	.dev_rx_b_baud_div   (cmd_proc_dev_rx_b_baud_div),
	.dev_rx_b_baud_fract (cmd_proc_dev_rx_b_baud_fract),
	.host_tx_reset       (host_tx_reset),
	.host_tx_baud_div    (host_tx_baud),
	.host_rx_reset       (host_rx_reset),
	.host_rx_baud_div    (host_rx_baud),
	.host_rx_baud_fract  (host_rx_fract));

wire [31:0] timer;
timer timer32(
	.clock (clock),
	.reset (reset),
	.timer (timer));

wire       dev_tx_a_active;
wire       dev_tx_a_ts_ready;
wire [7:0] dev_tx_a_ts_data;
wire       dev_tx_a_ts_read;
wire       dev_tx_a_ts_error;
wire       dev_tx_a_ts_fifo_full;
wire       dev_tx_a_ts_fifo_error;
wire       dev_tx_a_fifo_full;
wire       dev_tx_a_fifo_error;
wire       dev_tx_a_timestamp_error;
serial_tx_ts_fifo dev_tx_a(
	.clock           (clock),
	.reset           (cmd_proc_dev_tx_a_reset),
	.timer           (timer),
	.baud            (cmd_proc_dev_tx_a_baud_div),
	.tx              (rs485_a_tx),
	.byte            (cmd_proc_out_data_a),
	.byte_valid      (cmd_proc_out_data_a_valid),
	.active          (dev_tx_a_active),
	.ts_ready        (dev_tx_a_ts_ready),
	.ts_data         (dev_tx_a_ts_data),
	.ts_read         (dev_tx_a_ts_read),
	.ts_error        (dev_tx_a_ts_error),
	.ts_fifo_full    (dev_tx_a_ts_fifo_full),
	.ts_fifo_error   (dev_tx_a_ts_fifo_error),
	.tx_fifo_full    (dev_tx_a_fifo_full),
	.tx_fifo_error   (dev_tx_a_fifo_error),
	.timestamp_error (dev_tx_a_timestamp_error));
assign rs485_a_de = dev_tx_a_active;
assign rs485_a_re = 0;

wire       dev_tx_b_active;
wire       dev_tx_b_ts_ready;
wire [7:0] dev_tx_b_ts_data;
wire       dev_tx_b_ts_read;
wire       dev_tx_b_ts_error;
wire       dev_tx_b_ts_fifo_full;
wire       dev_tx_b_ts_fifo_error;
wire       dev_tx_b_fifo_full;
wire       dev_tx_b_fifo_error;
wire       dev_tx_b_timestamp_error;
serial_tx_ts_fifo dev_tx_b(
	.clock           (clock),
	.reset           (cmd_proc_dev_tx_b_reset),
	.timer           (timer),
	.baud            (cmd_proc_dev_tx_b_baud_div),
	.tx              (rs485_b_tx),
	.byte            (cmd_proc_out_data_b),
	.byte_valid      (cmd_proc_out_data_b_valid),
	.active          (dev_tx_b_active),
	.ts_ready        (dev_tx_b_ts_ready),
	.ts_data         (dev_tx_b_ts_data),
	.ts_read         (dev_tx_b_ts_read),
	.ts_error        (dev_tx_b_ts_error),
	.ts_fifo_full    (dev_tx_b_ts_fifo_full),
	.ts_fifo_error   (dev_tx_b_ts_fifo_error),
	.tx_fifo_full    (dev_tx_b_fifo_full),
	.tx_fifo_error   (dev_tx_b_fifo_error),
	.timestamp_error (dev_tx_b_timestamp_error));
assign rs485_b_de = dev_tx_b_active;
assign rs485_b_re = 0;

wire       dev_rx_a_active;
wire       dev_rx_a_ts_ready;
wire [7:0] dev_rx_a_ts_data;
wire       dev_rx_a_ts_read;
wire       dev_rx_a_ts_error;
wire       dev_rx_a_ts_fifo_full;
wire       dev_rx_a_ts_fifo_error;
wire       dev_rx_a_timestamp_error;
wire       dev_rx_a_stop_error;
serial_rx_ts dev_rx_a(
	.clock           (clock),
	.reset           (cmd_proc_dev_rx_a_reset),
	.timer           (timer),
	.baud            (cmd_proc_dev_rx_a_baud_div),
	.fract           (cmd_proc_dev_rx_a_baud_fract),
	.rx              (rs485_a_rx),
	.tx_mask         (dev_tx_a_active | !cmd_proc_dev_rx_a_enable),
	.ts_ready        (dev_rx_a_ts_ready),
	.ts_data         (dev_rx_a_ts_data),
	.ts_read         (dev_rx_a_ts_read),
	.ts_error        (dev_rx_a_ts_error),
	.ts_fifo_full    (dev_rx_a_ts_fifo_full),
	.ts_fifo_error   (dev_rx_a_ts_fifo_error),
	.stop_error      (dev_rx_a_stop_error));

wire       dev_rx_b_active;
wire       dev_rx_b_ts_ready;
wire [7:0] dev_rx_b_ts_data;
wire       dev_rx_b_ts_read;
wire       dev_rx_b_ts_error;
wire       dev_rx_b_ts_fifo_full;
wire       dev_rx_b_ts_fifo_error;
wire       dev_rx_b_timestamp_error;
wire       dev_rx_b_stop_error;
serial_rx_ts dev_rx_b(
	.clock           (clock),
	.reset           (cmd_proc_dev_rx_b_reset),
	.timer           (timer),
	.baud            (cmd_proc_dev_rx_b_baud_div),
	.fract           (cmd_proc_dev_rx_b_baud_fract),
	.rx              (rs485_b_rx),
	.tx_mask         (dev_tx_b_active | !cmd_proc_dev_rx_b_enable),
	.ts_ready        (dev_rx_b_ts_ready),
	.ts_data         (dev_rx_b_ts_data),
	.ts_read         (dev_rx_b_ts_read),
	.ts_error        (dev_rx_b_ts_error),
	.ts_fifo_full    (dev_rx_b_ts_fifo_full),
	.ts_fifo_error   (dev_rx_b_ts_fifo_error),
	.stop_error      (dev_rx_b_stop_error));

wire [31:0] error_mask;
wire        error_mask_change;
wire        error_mask_read;
error_mask error(
	.clock                    (clock),
	.reset                    (reset),
	.mask                     (error_mask),
	.mask_change              (error_mask_change),
	.mask_read                (error_mask_read),
	.dev_rx_a_stop_error      (dev_rx_a_stop_error),
	.dev_rx_b_stop_error      (dev_rx_b_stop_error),
	.dev_rx_a_ts_error        (dev_rx_a_timestamp_error),
	.dev_rx_b_ts_error        (dev_rx_b_timestamp_error),
	.dev_rx_a_ts_fifo_error   (dev_rx_a_ts_fifo_error),
	.dev_rx_b_ts_fifo_error   (dev_rx_b_ts_fifo_error),
	.dev_tx_a_fifo_error      (dev_tx_a_fifo_error),
	.dev_tx_b_fifo_error      (dev_tx_b_fifo_error),
	.dev_tx_a_ts_error        (dev_tx_a_ts_error),
	.dev_tx_b_ts_error        (dev_tx_b_ts_error),
	.dev_tx_a_ts_fifo_error   (dev_tx_a_ts_fifo_error),
	.dev_tx_b_ts_fifo_error   (dev_tx_b_ts_fifo_error),
	.host_rx_stop_error       (host_rx_stop_error),
	.cmd_fifo_error           (cmd_proc_cmd_fifo_error),
	.cmd_serializer_error     (cmd_proc_cmd_serialize_error));

wire       error_serializer_ready;
wire [7:0] error_serializer_data;
wire       error_serializer_read;
error_serialize error_serializer(
	.clock       (clock),
	.reset       (reset),
	.enable      (cmd_proc_error_enable),
	.mask        (error_mask),
	.mask_change (error_mask_change),
	.mask_read   (error_mask_read),
	.byte_valid  (error_serializer_ready),
	.byte        (error_serializer_data),
	.byte_read   (error_serializer_read));

host_tx_arbiter arbiter(
	.clock         (clock),
	.reset         (reset),
	.tx_a_ts_full  (dev_tx_a_ts_fifo_full),
	.tx_a_ts_ready (dev_tx_a_ts_ready),
	.tx_a_ts_data  (dev_tx_a_ts_data),
	.tx_a_ts_read  (dev_tx_a_ts_read),
	.tx_b_ts_full  (dev_tx_b_ts_fifo_full),
	.tx_b_ts_ready (dev_tx_b_ts_ready),
	.tx_b_ts_data  (dev_tx_b_ts_data),
	.tx_b_ts_read  (dev_tx_b_ts_read),
	.rx_a_full     (dev_rx_a_ts_fifo_full),
	.rx_a_ready    (dev_rx_a_ts_ready),
	.rx_a_data     (dev_rx_a_ts_data),
	.rx_a_read     (dev_rx_a_ts_read),
	.rx_b_full     (dev_rx_b_ts_fifo_full),
	.rx_b_ready    (dev_rx_b_ts_ready),
	.rx_b_data     (dev_rx_b_ts_data),
	.rx_b_read     (dev_rx_b_ts_read),
	.cmd_full      (cmd_proc_cmd_fifo_full),
	.cmd_ready     (cmd_proc_cmd_fifo_ready),
	.cmd_data      (cmd_proc_cmd_fifo_data),
	.cmd_read      (cmd_proc_cmd_fifo_read),
	.error_ready   (error_serializer_ready),
	.error_data    (error_serializer_data),
	.error_read    (error_serializer_read),
	.out_read      (host_tx_start_byte),
	.out_valid     (host_tx_byte_valid),
	.out_data      (host_tx_byte));

assign led[0] = !host_rx_stop_error & !cmd_proc_cmd_fifo_error & !cmd_proc_cmd_serialize_error;
assign led[1] = dev_rx_a_ts_fifo_error | dev_rx_a_timestamp_error | dev_rx_a_stop_error;
assign led[2] = dev_rx_b_ts_fifo_error | dev_rx_b_timestamp_error | dev_rx_b_stop_error;
assign led[3] = dev_tx_a_ts_error | dev_tx_a_ts_fifo_error | dev_tx_a_fifo_error;
assign led[4] = dev_tx_b_ts_error | dev_tx_b_ts_fifo_error | dev_tx_b_fifo_error;

assign rs232_rts_n = (cmd_proc_cmd_fifo_full
	| dev_tx_a_fifo_full | dev_tx_b_fifo_full);

endmodule
