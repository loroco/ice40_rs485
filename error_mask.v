module error_mask(
	clock, reset,
	mask, mask_change, mask_read,
	dev_rx_a_stop_error,
	dev_rx_b_stop_error,
	dev_rx_a_ts_error,
	dev_rx_b_ts_error,
	dev_rx_a_ts_fifo_error,
	dev_rx_b_ts_fifo_error,
	dev_tx_a_fifo_error,
	dev_tx_b_fifo_error,
	dev_tx_a_ts_error,
	dev_tx_b_ts_error,
	dev_tx_a_ts_fifo_error,
	dev_tx_b_ts_fifo_error,
	host_rx_stop_error,
	cmd_fifo_error,
	cmd_serializer_error);

input wire clock;
input wire reset;

output reg [31:0] mask;
output reg        mask_change;
input wire        mask_read;

input wire dev_rx_a_stop_error;
input wire dev_rx_b_stop_error;
input wire dev_rx_a_ts_error;
input wire dev_rx_b_ts_error;
input wire dev_rx_a_ts_fifo_error;
input wire dev_rx_b_ts_fifo_error;
input wire dev_tx_a_fifo_error;
input wire dev_tx_b_fifo_error;
input wire dev_tx_a_ts_error;
input wire dev_tx_b_ts_error;
input wire dev_tx_a_ts_fifo_error;
input wire dev_tx_b_ts_fifo_error;
input wire host_rx_stop_error;
input wire cmd_fifo_error;
input wire cmd_serializer_error;

wire [31:0] changes;
assign changes[ 0] = dev_rx_a_stop_error;
assign changes[ 1] = dev_rx_b_stop_error;
assign changes[ 2] = dev_rx_a_ts_error;
assign changes[ 3] = dev_rx_b_ts_error;
assign changes[ 4] = dev_rx_a_ts_fifo_error;
assign changes[ 5] = dev_rx_b_ts_fifo_error;
assign changes[ 6] = 1'b0;
assign changes[ 7] = 1'b0;

assign changes[ 8] = dev_tx_a_fifo_error;
assign changes[ 9] = dev_tx_b_fifo_error;
assign changes[10] = dev_tx_a_ts_error;
assign changes[11] = dev_tx_b_ts_error;
assign changes[12] = dev_tx_a_ts_fifo_error;
assign changes[13] = dev_tx_b_ts_fifo_error;
assign changes[14] = 1'b0;
assign changes[15] = 1'b0;

assign changes[16] = host_rx_stop_error;
assign changes[17] = cmd_fifo_error;
assign changes[18] = cmd_serializer_error;
assign changes[31:15] = 13'h0000;

wire [31:0] delta = changes & ~mask;

initial
begin
	mask        <= 0;
	mask_change <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		mask        <= 0;
		mask_change <= 0;
	end
	else
	begin
		mask <= (mask | changes);

		if (delta != 0)
			mask_change <= 1;
		else if (mask_read)
			mask_change <= 0;
	end
end

endmodule
