#ifndef __ICE40_RS485_H__
#define __ICE40_RS485_H__

#include <stdint.h>
#include <stdbool.h>

static const char[2]  ICE40_RS485_IDENT   = "DT";
static const uint16_t ICE40_RS485_VERSION = 1;

typedef enum
{
	HOST_CMD_RESET      = 0,
	HOST_CMD_RESET_SOFT = 1,

	HOST_CMD_GET_BASE_CLOCK = 2,

	HOST_CMD_SET_FLAGS = 3,

	HOST_CMD_SET_RX_BAUD = 4,
	HOST_CMD_SET_TX_BAUD = 5,

	HOST_CMD_SET_DEV_TX_BAUD_A =  8,
	HOST_CMD_SET_DEV_RX_BAUD_A =  9,
	HOST_CMD_SET_DEV_TX_BAUD_B = 10,
	HOST_CMD_SET_DEV_RX_BAUD_B = 11,

	HOST_CMD_SEND_DATA_A = 16,
	HOST_CMD_SEND_DATA_B = 17,
} host_cmd_e;

typedef struct
__attribute__((__packed__))
{
	host_cmd_e cmd;

	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			bool     enable_a : 1;
			bool     enable_b : 1;
			unsigned reserved : 6;
		} flags;

		uint16_t tx_baud;

		struct
		__attribute__((__packed__))
		{
			uint16_t baud;
			uint16_t fract;
		} rx_baud;

		struct
		__attribute__((__packed__))
		{
			uint8_t size;
			uint8_t data[256];
		} send;
	};
} host_cmd_t;


unsigned host_cmd_reset(host_cmd_t* cmd, bool soft);
unsigned host_cmd_get_base_clock(host_cmd_t* cmd);
unsigned host_cmd_set_flags(bool enable_a, bool enable_b, host_cmd_t* cmd);
unsigned host_cmd_set_rx_baud(double baud, host_cmd_t* cmd);
unsigned host_cmd_set_tx_baud(double baud, host_cmd_t* cmd);
unsigned host_cmd_set_dev_rx_baud(unsigned port, double baud, host_cmd_t* cmd);
unsigned host_cmd_set_dev_tx_baud(unsigned port, double baud, host_cmd_t* cmd);
unsigned host_cmd_send_data(unsigned port, unsigned size, const uint8_t data[], host_cmd_t* cmd);

typedef enum
{
	DEV_CMD_RESET_ACK  = 0,
	DEV_CMD_BASE_CLOCK = 1,

	DEV_CMD_RECV_DATA_A = 2,
	DEV_CMD_RECV_DATA_B = 3,

	DEV_CMD_TIMESTAMP_TX_A = 4,
	DEV_CMD_TIMESTAMP_TX_B = 5,

	DEV_CMD_ERROR = 0xFF,
} dev_cmd_e;

typedef struct
__attribute__((__packed__))
{
	uint8_t cmd; /* dev_cmd_e */

	union
	__attribute__((__packed__))
	{
		struct
		__attribute__((__packed__))
		{
			char     ident[2];
			uint16_t version;
		} reset_ack;

		uint32_t base_clock;

		struct
		__attribute__((__packed__))
		{
			uint32_t start, end;
		} timestamp;

		struct
		__attribute__((__packed__))
		{
			uint32_t start, end;
			uint8_t  data;
		} recv

		union
		__attribute__((__packed__))
		{
			struct
			__attribute__((__packed__))
			{
				bool     dev_rx_a_stop_error      :  1;
				bool     dev_rx_b_stop_error      :  1;
				bool     dev_rx_a_ts_error        :  1;
				bool     dev_rx_b_ts_error        :  1;
				bool     dev_rx_a_ts_fifo_error   :  1;
				bool     dev_rx_b_ts_fifo_error   :  1;
				unsigned dev_rx_reserved          :  2;
				bool     dev_tx_a_fifo_error      :  1;
				bool     dev_tx_b_fifo_error      :  1;
				bool     dev_tx_a_ts_error        :  1;
				bool     dev_tx_b_ts_error        :  1;
				bool     dev_tx_a_ts_fifo_error   :  1;
				bool     dev_tx_b_ts_fifo_error   :  1;
				unsigned dev_tx_reserved          :  2;
				bool     host_rx_stop_error       :  1;
				bool     cmd_fifo_error           :  1;
				bool     cmd_serializer_error     :  1;
				unsigned reserved                 : 13;
			};

			uint32_t mask;
		} error;
	};
} dev_cmd_t;

#endif
