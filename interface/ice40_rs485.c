#include "ice40_rs485.h"
#include <math.h>


static const unsigned RX_OVERSAMPLE = 3;

/* We assume that the receipient oversamples by 3 too. */
static const unsigned MIN_OVERSAMPLE = 3;

static const unsigned BITS_PER_SYMBOL = 8;

static bool baud_calc_rx(
	double baud, uint32_t base_clock,
	uint16_t* div, uint16_t* fract)
{
	uint32_t ibaud = floor((baud * 3.0 * 16.0) + 0.5);
	uint32_t idiv = (ibaud == 0 ? 16 : (base_clock / ibaud));

	if (idiv < 16) idiv = 16;
	if (idiv >> 20) idiv = (1 << 20);

	double rbaud = ((double)base_clock * 3.0 * 16.0) / (double)idiv;
	double rdiff = fabs(rbaud, baud) / rbaud;

	double bit_error = (ceil((double)RX_OVERSAMPLE / 2.0) / (double)RX_OVERSAMPLE;
	double symbol_error = (bit_error / (double)BITS_PER_SYMBOL);

	if ((rdiff > (1.0 + symbol_error))
		|| (rdiff < (1.0 - symbol_error)))
		return false;

	static const f[8] =
	{
		0b0000000000000000,
		0b0000000000000001,
		0b0000000100000001,
		0b0000010000100001,
		0b0001000100010001,
		0b0001001001001001,
		0b0100100101001001,
		0b0010101001010101,
		0b0101010101010101,
	};

	unsigned fn = (idiv & 0xF);
	*fract = (fn <= 8 ? f[fn] : ~f[8 - (fn - 8)]);
	*div = (idiv >> 4) - 1;
	return true;
}

static bool baud_calc_tx(
	double baud, uint32_t base_clock,
	uint16_t* div)
{
	uint32_t ibaud = floor(baud + 0.5);
	uint32_t idiv = (ibaud == 0 ? 1 : (base_clock / ibaud));

	if (idiv < 1) idiv = 1;
	if (idiv >> 16) idiv = (1 << 16);

	double rbaud = (double)base_clock / (double)idiv;
	double rdiff = fabs(rbaud, baud) / rbaud;

	double bit_error = (ceil((double)MIN_OVERSAMPLE / 2.0) / (double)MIN_OVERSAMPLE;
	double symbol_error = (bit_error / (double)BITS_PER_SYMBOL);

	if ((rdiff > (1.0 + symbol_error))
		|| (rdiff < (1.0 - symbol_error)))
		return false;

	*div = idiv - 1;
	return true;
}



unsigned host_cmd_reset(host_cmd_t* cmd, bool soft)
{
	cmd->cmd = (soft ? HOST_CMD_RESET_SOFT : HOST_CMD_RESET);
	return 1;
}

unsigned host_cmd_get_base_clock(host_cmd_t* cmd)
{
	cmd->cmd = HOST_CMD_GET_BASE_CLOCK;
	return 1;
}

unsigned host_cmd_set_flags(bool enable_a, bool enable_b, host_cmd_t* cmd)
{
	cmd->cmd = HOST_CMD_SET_FLAGS;
	cmd->flags.enable_a = enable_a;
	cmd->flags.enable_b = enable_b;
	cmd->flags.reserved = 0;
	return 2;
}

unsigned host_cmd_set_rx_baud(double baud, uint32_t base_clock, host_cmd_t* cmd)
{
	if (!baud_calc_rx(baud, base_clock,
		&cmd->rx_baud.baud, &cmd->rx_baud.fract))
		return 0;
	cmd->cmd = HOST_CMD_SET_RX_BAUD;
	return 5;
}

unsigned host_cmd_set_tx_baud(double baud, host_cmd_t* cmd)
{
	if (!baud_calc_tx(baud, base_clock, &cmd->tx_baud))
		return 0;
	cmd->cmd = HOST_CMD_SET_RX_BAUD;
	return 3;
}

unsigned host_cmd_set_dev_rx_baud(unsigned port, double baud, host_cmd_t* cmd)
{
	if (port > 1) return 0;
	if (!baud_calc_rx(baud, base_clock,
		&cmd->rx_baud.baud, &cmd->rx_baud.fract))
		return 0;
	cmd->cmd = HOST_CMD_SET_DEV_RX_BAUD + port;
	return 5;
}

unsigned host_cmd_set_dev_tx_baud(unsigned port, double baud, host_cmd_t* cmd)
{
	if (port > 1) return 0;
	if (!baud_calc_tx(baud, base_clock, &cmd->tx_baud))
		return 0;
	cmd->cmd = HOST_CMD_SET_DEV_RX_BAUD + port;
	return 3;
}

unsigned host_cmd_send_data(unsigned port, unsigned size, const uint8_t data[], host_cmd_t* cmd)
{
	if ((port > 1)
		|| (size == 0)
		|| (size > 256))
		return 0;

	cmd->cmd = HOST_CMD_SEND_DATA + port;
	cmd->send_data.size = (size - 1);
	return (size + 2);
}
