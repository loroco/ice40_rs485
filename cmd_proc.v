module cmd_proc(
	clock, reset,
	in_data_valid, in_data,
	out_data_a_valid, out_data_a,
	out_data_b_valid, out_data_b,
	out_cmd_valid, out_cmd_id, out_cmd_data,
	error_enable,
	dev_tx_a_reset, dev_tx_a_baud_div,
	dev_rx_a_reset, dev_rx_a_enable, dev_rx_a_baud_div, dev_rx_a_baud_fract,
	dev_tx_b_reset, dev_tx_b_baud_div,
	dev_rx_b_reset, dev_rx_b_enable, dev_rx_b_baud_div, dev_rx_b_baud_fract,
	host_tx_reset, host_tx_baud_div,
	host_rx_reset, host_rx_baud_div, host_rx_baud_fract);

input  wire clock;
output reg  reset;

input wire       in_data_valid;
input wire [7:0] in_data;

output reg       out_data_a_valid;
output reg [7:0] out_data_a;

output reg       out_data_b_valid;
output reg [7:0] out_data_b;

output reg        out_cmd_valid;
output reg [ 7:0] out_cmd_id;
output reg [31:0] out_cmd_data;

output reg        error_enable;

output reg        dev_tx_a_reset;
output reg [15:0] dev_tx_a_baud_div;
output reg        dev_rx_a_reset;
output reg        dev_rx_a_enable;
output reg [15:0] dev_rx_a_baud_div;
output reg [15:0] dev_rx_a_baud_fract;

output reg        dev_tx_b_reset;
output reg [15:0] dev_tx_b_baud_div;
output reg        dev_rx_b_reset;
output reg        dev_rx_b_enable;
output reg [15:0] dev_rx_b_baud_div;
output reg [15:0] dev_rx_b_baud_fract;

output reg        host_tx_reset;
output reg [15:0] host_tx_baud_div;
output reg        host_rx_reset;
output reg [15:0] host_rx_baud_div;
output reg [15:0] host_rx_baud_fract;


reg [ 3:0] mode;
reg [ 7:0] count;
reg [23:0] fifo;

wire [31:0] fifo_word = {in_data,fifo};

`define CLOCK_RATE          12000000
`define INIT_DEV_BAUD_RATE      9600
`define INIT_HOST_BAUD_RATE   115200
// Must be hand calculated for initial rate against clock.
`define INIT_DEV_BAUD_FRACT  16'b0110110110110111
`define INIT_HOST_BAUD_FRACT 16'b1110111011101110


`define DEV_CMD_RESET_ACK  8'h00
`define DEV_CMD_BASE_CLOCK 8'h01

`define CMD_RESET               8'h00
`define CMD_RESET_SOFT          8'h01
`define CMD_GET_BASE_CLOCK      8'h02
`define CMD_SET_FLAGS           8'h03
`define CMD_SET_HOST_TX_BAUD    8'h04
`define CMD_SET_HOST_RX_BAUD    8'h05
`define CMD_SET_DEV_TX_BAUD_A   8'h08
`define CMD_SET_DEV_RX_BAUD_A   8'h09
`define CMD_SET_DEV_TX_BAUD_B   8'h0A
`define CMD_SET_DEV_RX_BAUD_B   8'h0B
`define CMD_SEND_DATA_A         8'h10
`define CMD_SEND_DATA_B         8'h11

`define MODE_NONE                4'h0
`define MODE_SET_FLAGS           4'h1
`define MODE_SET_HOST_TX_BAUD    4'h2
`define MODE_SET_HOST_RX_BAUD    4'h3
`define MODE_SEND_SIZE_A         4'h4
`define MODE_SEND_SIZE_B         4'h5
`define MODE_SEND_DATA_A         4'h6
`define MODE_SEND_DATA_B         4'h7
`define MODE_SET_DEV_TX_BAUD_A   4'h8
`define MODE_SET_DEV_TX_BAUD_B   4'h9
`define MODE_SET_DEV_RX_BAUD_A   4'hA
`define MODE_SET_DEV_RX_BAUD_B   4'hB

`define PROTOCOL_VERSION 1
`define PROTOCOL_IDENT   16'h5444


initial
begin
	out_data_a_valid <= 0;
	out_data_b_valid <= 0;
	out_cmd_valid    <= 0;

	error_enable <= 0;

	dev_tx_a_baud_div   <= (`CLOCK_RATE / `INIT_DEV_BAUD_RATE);
	dev_rx_a_baud_div   <= (`CLOCK_RATE / (`INIT_DEV_BAUD_RATE * 3));
	dev_rx_a_baud_fract <= `INIT_DEV_BAUD_FRACT;

	dev_tx_b_baud_div   <= (`CLOCK_RATE / `INIT_DEV_BAUD_RATE);
	dev_rx_b_baud_div   <= (`CLOCK_RATE / (`INIT_DEV_BAUD_RATE * 3));
	dev_rx_b_baud_fract <= `INIT_DEV_BAUD_FRACT;

	host_tx_baud_div   <= (`CLOCK_RATE / `INIT_HOST_BAUD_RATE);
	host_rx_baud_div   <= (`CLOCK_RATE / (`INIT_HOST_BAUD_RATE * 3));
	host_rx_baud_fract <= `INIT_HOST_BAUD_FRACT;

	dev_tx_a_reset  <= 0;
	dev_tx_b_reset  <= 0;
	dev_rx_a_reset  <= 0;
	dev_rx_a_enable <= 0;
	dev_rx_b_reset  <= 0;
	dev_rx_b_enable <= 0;
	host_tx_reset   <= 0;
	host_rx_reset   <= 0;
	reset           <= 0;

	mode <= `MODE_NONE;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		out_data_a_valid <= 0;
		out_data_b_valid <= 0;

		out_cmd_valid       <= 1;
		out_cmd_id          <= `DEV_CMD_RESET_ACK;
		out_cmd_data[31:16] <= `PROTOCOL_VERSION;
		out_cmd_data[15: 0] <= `PROTOCOL_IDENT;

		dev_tx_a_reset <= 0;
		dev_tx_b_reset <= 0;
		dev_rx_a_reset <= 0;
		dev_rx_b_reset <= 0;
		host_tx_reset  <= 0;
		host_rx_reset  <= 0;
		reset          <= 0;

		mode <= `MODE_NONE;
	end
	else if (in_data_valid)
	begin
		fifo <= {in_data,fifo[23:8]};

		case (mode)
			`MODE_NONE:
			begin
				case (in_data)
					`CMD_RESET:
					begin
						reset         <= 1;
						out_cmd_valid <= 0;

						dev_rx_a_enable <= 0;
						dev_rx_b_enable <= 0;

						error_enable <= 0;

						dev_tx_a_baud_div   <= (`CLOCK_RATE / `INIT_DEV_BAUD_RATE);
						dev_rx_a_baud_div   <= (`CLOCK_RATE / (`INIT_DEV_BAUD_RATE * 3));
						dev_rx_a_baud_fract <= `INIT_DEV_BAUD_FRACT;

						dev_tx_b_baud_div   <= (`CLOCK_RATE / `INIT_DEV_BAUD_RATE);
						dev_rx_b_baud_div   <= (`CLOCK_RATE / (`INIT_DEV_BAUD_RATE * 3));
						dev_rx_b_baud_fract <= `INIT_DEV_BAUD_FRACT;

						host_tx_baud_div   <= (`CLOCK_RATE / `INIT_HOST_BAUD_RATE);
						host_rx_baud_div   <= (`CLOCK_RATE / (`INIT_HOST_BAUD_RATE * 3));
						host_rx_baud_fract <= `INIT_HOST_BAUD_FRACT;
					end

					`CMD_RESET_SOFT:
					begin
						reset         <= 1;
						out_cmd_valid <= 0;
					end

					`CMD_GET_BASE_CLOCK:
					begin
						out_cmd_valid <= 1;
						out_cmd_id    <= `DEV_CMD_BASE_CLOCK;
						out_cmd_data  <= `CLOCK_RATE;
					end

					`CMD_SET_FLAGS:
					begin
						out_cmd_valid <= 0;
						mode <= `MODE_SET_FLAGS;
					end

					`CMD_SET_DEV_TX_BAUD_A:
					begin
						out_cmd_valid <= 0;
						count <= 1;
						mode <= `MODE_SET_DEV_TX_BAUD_A;
					end

					`CMD_SET_DEV_TX_BAUD_B:
					begin
						out_cmd_valid <= 0;
						count <= 1;
						mode <= `MODE_SET_DEV_TX_BAUD_B;
					end

					`CMD_SET_DEV_RX_BAUD_A:
					begin
						out_cmd_valid <= 0;
						count <= 3;
						mode <= `MODE_SET_DEV_RX_BAUD_A;
					end

					`CMD_SET_DEV_RX_BAUD_B:
					begin
						out_cmd_valid <= 0;
						count <= 3;
						mode <= `MODE_SET_DEV_RX_BAUD_B;
					end

					`CMD_SET_HOST_TX_BAUD:
					begin
						out_cmd_valid <= 0;
						count <= 1;
						mode <= `MODE_SET_HOST_TX_BAUD;
					end

					`CMD_SET_HOST_RX_BAUD:
					begin
						out_cmd_valid <= 0;
						count <= 3;
						mode <= `MODE_SET_HOST_RX_BAUD;
					end

					`CMD_SEND_DATA_A:
					begin
						out_cmd_valid <= 0;
						mode <= `MODE_SEND_SIZE_A;
					end

					`CMD_SEND_DATA_B:
					begin
						out_cmd_valid <= 0;
						mode <= `MODE_SEND_SIZE_B;
					end

					default:
					begin
						// TODO - Set error mask.
						out_cmd_valid <= 0;
					end
				endcase

				out_data_a_valid <= 0;
				out_data_b_valid <= 0;

				dev_tx_a_reset <= ((mode == `CMD_RESET) || (mode == `CMD_RESET_SOFT));
				dev_tx_b_reset <= ((mode == `CMD_RESET) || (mode == `CMD_RESET_SOFT));
				dev_rx_a_reset <= ((mode == `CMD_RESET) || (mode == `CMD_RESET_SOFT));
				dev_rx_b_reset <= ((mode == `CMD_RESET) || (mode == `CMD_RESET_SOFT));
				host_tx_reset  <= ((mode == `CMD_RESET) || (mode == `CMD_RESET_SOFT));
				host_rx_reset  <= ((mode == `CMD_RESET) || (mode == `CMD_RESET_SOFT));
			end

			`MODE_SEND_SIZE_A:
			begin
				count <= in_data;
				mode  <= `MODE_SEND_DATA_A;
			end
			
			`MODE_SEND_SIZE_B:
			begin
				count <= in_data;
				mode  <= `MODE_SEND_DATA_B;
			end

			`MODE_SEND_DATA_A:
			begin
				out_data_a_valid <= 1;
				out_data_a       <= in_data;
				count            <= (count - 1);
				if (count == 0) mode <= `MODE_NONE;
			end

			`MODE_SEND_DATA_B:
			begin
				out_data_b_valid <= 1;
				out_data_b       <= in_data;
				count            <= (count - 1);
				if (count == 0) mode <= `MODE_NONE;
			end


			`MODE_SET_FLAGS:
			begin
				dev_rx_a_enable <= in_data[0];
				dev_rx_b_enable <= in_data[1];
				error_enable    <= in_data[7];
				mode <= `MODE_NONE;
			end

			`MODE_SET_HOST_TX_BAUD:
			begin
				if (count == 0)
				begin
					host_tx_reset    <= 1;
					host_tx_baud_div <= fifo_word[31:16];
					mode <= `MODE_NONE;
				end
				else
				begin
					count <= (count - 1);
				end
			end

			`MODE_SET_HOST_RX_BAUD:
			if (count == 0)
			begin
				host_rx_reset      <= 1;
				host_rx_baud_div   <= fifo_word[15: 0];
				host_rx_baud_fract <= fifo_word[31:16];
				mode <= `MODE_NONE;
			end
			else
			begin
				count <= (count - 1);
			end

			`MODE_SET_DEV_TX_BAUD_A:
			begin
				if (count == 0)
				begin
					dev_tx_a_reset    <= 1;
					dev_tx_a_baud_div <= fifo_word[31:16];
					mode <= `MODE_NONE;
				end
				else
				begin
					count <= (count - 1);
				end
			end

			`MODE_SET_DEV_TX_BAUD_B:
			begin
				if (count == 0)
				begin
					dev_tx_b_reset    <= 1;
					dev_tx_b_baud_div <= fifo_word[31:16];
					mode <= `MODE_NONE;
				end
				else
				begin
					count <= (count - 1);
				end
			end

			`MODE_SET_DEV_RX_BAUD_A:
			if (count == 0)
			begin
				dev_rx_a_reset      <= 1;
				dev_rx_a_baud_div   <= fifo_word[15: 0];
				dev_rx_a_baud_fract <= fifo_word[31:16];
				mode <= `MODE_NONE;
			end
			else
			begin
				count <= (count - 1);
			end

			`MODE_SET_DEV_RX_BAUD_B:
			if (count == 0)
			begin
				dev_rx_b_reset      <= 1;
				dev_rx_b_baud_div   <= fifo_word[15: 0];
				dev_rx_b_baud_fract <= fifo_word[31:16];
				mode <= `MODE_NONE;
			end
			else
			begin
				count <= (count - 1);
			end
		endcase
	end
	else
	begin
		dev_tx_a_reset <= 0;
		dev_rx_a_reset <= 0;
		dev_tx_b_reset <= 0;
		dev_rx_b_reset <= 0;
		host_tx_reset  <= 0;
		host_rx_reset  <= 0;

		out_data_a_valid <= 0;
		out_data_b_valid <= 0;
		out_cmd_valid    <= 0;
	end
end

endmodule
