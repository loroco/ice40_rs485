module cmd_proc_fifo(
	clock, reset,
	in_data_valid, in_data,
	out_data_a_valid, out_data_a,
	out_data_b_valid, out_data_b,
	cmd_fifo_data, cmd_fifo_ready, cmd_fifo_full, cmd_fifo_read,
	cmd_fifo_error, cmd_serialize_error,
	error_enable,
	dev_tx_a_reset, dev_tx_a_baud_div,
	dev_rx_a_reset, dev_rx_a_enable, dev_rx_a_baud_div, dev_rx_a_baud_fract,
	dev_tx_b_reset, dev_tx_b_baud_div,
	dev_rx_b_reset, dev_rx_b_enable, dev_rx_b_baud_div, dev_rx_b_baud_fract,
	host_tx_reset, host_tx_baud_div,
	host_rx_reset, host_rx_baud_div, host_rx_baud_fract);

input  wire clock;
output wire reset;

input wire       in_data_valid;
input wire [7:0] in_data;

output wire       out_data_a_valid;
output wire [7:0] out_data_a;

output wire       out_data_b_valid;
output wire [7:0] out_data_b;

output wire [7:0] cmd_fifo_data;
output wire       cmd_fifo_ready;
output wire       cmd_fifo_full;
input  wire       cmd_fifo_read;

output wire cmd_fifo_error;
output wire cmd_serialize_error;

output wire error_enable;

output wire        dev_tx_a_reset;
output wire [15:0] dev_tx_a_baud_div;
output wire        dev_rx_a_reset;
output wire        dev_rx_a_enable;
output wire [15:0] dev_rx_a_baud_div;
output wire [15:0] dev_rx_a_baud_fract;

output wire        dev_tx_b_reset;
output wire [15:0] dev_tx_b_baud_div;
output wire        dev_rx_b_reset;
output wire        dev_rx_b_enable;
output wire [15:0] dev_rx_b_baud_div;
output wire [15:0] dev_rx_b_baud_fract;

output wire        host_tx_reset;
output wire [15:0] host_tx_baud_div;
output wire        host_rx_reset;
output wire [15:0] host_rx_baud_div;
output wire [15:0] host_rx_baud_fract;


wire        cmd_proc_cmd_valid;
wire [ 7:0] cmd_proc_cmd_id;
wire [31:0] cmd_proc_cmd_data;
cmd_proc cmd_proc(
	.clock               (clock),
	.reset               (reset),
	.in_data_valid       (in_data_valid),
	.in_data             (in_data),
	.out_data_a_valid    (out_data_a_valid),
	.out_data_a          (out_data_a),
	.out_data_b_valid    (out_data_b_valid),
	.out_data_b          (out_data_b),
	.out_cmd_valid       (cmd_proc_cmd_valid),
	.out_cmd_id          (cmd_proc_cmd_id),
	.out_cmd_data        (cmd_proc_cmd_data),
	.error_enable        (error_enable),
	.dev_tx_a_reset      (dev_tx_a_reset),
	.dev_tx_a_baud_div   (dev_tx_a_baud_div),
	.dev_rx_a_reset      (dev_rx_a_reset),
	.dev_rx_a_enable     (dev_rx_a_enable),
	.dev_rx_a_baud_div   (dev_rx_a_baud_div),
	.dev_rx_a_baud_fract (dev_rx_a_baud_fract),
	.dev_tx_b_reset      (dev_tx_b_reset),
	.dev_tx_b_baud_div   (dev_tx_b_baud_div),
	.dev_rx_b_reset      (dev_rx_b_reset),
	.dev_rx_b_enable     (dev_rx_b_enable),
	.dev_rx_b_baud_div   (dev_rx_b_baud_div),
	.dev_rx_b_baud_fract (dev_rx_b_baud_fract),
	.host_tx_reset       (host_tx_reset),
	.host_tx_baud_div    (host_tx_baud_div),
	.host_rx_reset       (host_rx_reset),
	.host_rx_baud_div    (host_rx_baud_div),
	.host_rx_baud_fract  (host_rx_baud_fract));

wire [7:0] serialize_byte;
wire       serialize_byte_ready;
cmd_serialize serialize(
	.clock      (clock),
	.reset      (reset),
	.cmd_id     (cmd_proc_cmd_id),
	.cmd_data   (cmd_proc_cmd_data),
	.cmd_ready  (cmd_proc_cmd_valid),
	.byte       (serialize_byte),
	.byte_ready (serialize_byte_ready),
	.error      (cmd_serialize_error));

wire fifo_empty;
byte_fifo cmd_fifo(
	.clock   (clock),
	.reset   (reset),
	.wr_en   (serialize_byte_ready),
	.wr_data (serialize_byte),
	.rd_en   (cmd_fifo_read),
	.rd_data (cmd_fifo_data),
	.empty   (fifo_empty),
	.full    (cmd_fifo_full),
	.error   (cmd_fifo_error));
assign cmd_fifo_ready = !fifo_empty;

endmodule
