module serial_tx_ts_fifo(
	clock, reset,
	timer,
	baud, tx,
	byte, byte_valid,
	active,
	ts_ready, ts_data, ts_read, ts_error,
	ts_fifo_full, ts_fifo_error,
	tx_fifo_full, tx_fifo_error,
	timestamp_error);

input wire clock;
input wire reset;

input wire [31:0] timer;

input  wire [15:0] baud;
output wire        tx;

input wire [7:0] byte;
input wire       byte_valid;

output wire active;

output wire       ts_ready;
output wire [7:0] ts_data;
input  wire       ts_read;
output wire       ts_error;

output wire ts_fifo_full;
output wire ts_fifo_error;

output wire tx_fifo_full;
output wire tx_fifo_error;

output wire timestamp_error;

wire start_byte;
wire end_byte;
serial_tx_fifo dev_tx(
	.clock      (clock),
	.reset      (reset),
	.baud       (baud),
	.tx         (tx),
	.byte       (byte),
	.byte_valid (byte_valid),
	.active     (active),
	.start_byte (start_byte),
	.end_byte   (end_byte),
	.fifo_full  (tx_fifo_full),
	.fifo_error (tx_fifo_error));

wire       ts_fifo_wr;
wire [7:0] ts_fifo_data;
timestamp_tx ts(
	.clock     (clock),
	.reset     (reset),
	.timer     (timer),
	.tx_start  (start_byte),
	.tx_end    (end_byte),
	.fifo_wr   (ts_fifo_wr),
	.fifo_data (ts_fifo_data),
	.error     (timestamp_error));

wire ts_fifo_empty;
byte_fifo ts_fifo(
	.clock   (clock),
	.reset   (reset),
	.wr_en   (ts_fifo_wr),
	.wr_data (ts_fifo_data),
	.rd_en   (ts_read),
	.rd_data (ts_data),
	.empty   (ts_fifo_empty),
	.full    (ts_fifo_full),
	.error   (ts_fifo_error));

assign ts_ready = !ts_fifo_empty;

endmodule
