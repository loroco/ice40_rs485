module serial_rx(
	clock, reset,
	baud, fract, rx,
	byte, byte_valid,
	stop_error,
	start_byte, end_byte);

input wire clock;
input wire reset;

input wire [15:0] baud;
input wire [15:0] fract;
input wire rx;

output reg [7:0] byte;
output reg       byte_valid;

output reg stop_error;

output reg start_byte;
output reg end_byte;


reg [15:0] clkdiv;
reg [ 3:0] fract_offset;
reg [ 1:0] sample;
reg [ 1:0] fifo;

wire [2:0] rx_full = { fifo, rx };

reg serial_bit_valid;
reg serial_bit;

initial
begin
	clkdiv           <= 0;
	fract_offset     <= 0;
	sample           <= 0;
	serial_bit_valid <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		clkdiv           <= 0;
		fract_offset     <= 0;
		sample           <= 0;
		serial_bit_valid <= 0;
	end
	else if (clkdiv >= (baud + fract[fract_offset]))
	begin
		clkdiv <= 0;
		fract_offset <= fract_offset + 1;
		fifo <= rx_full[1:0];

		if (sample >= 2)
		begin
			case (rx_full)
				 3'b100: sample <= 2;
				 3'b011: sample <= 2;
				 3'b110: sample <= 1;
				 3'b001: sample <= 1;
				default: sample <= 0;
			endcase

			case (rx_full)
				 3'b100: serial_bit_valid <= 0;
				 3'b011: serial_bit_valid <= 0;
				default: serial_bit_valid <= 1;
			endcase

			serial_bit <= (fifo[0] & fifo[1])
				| (fifo[0] & rx)
				| (fifo[1] & rx);
		end
		else
		begin
			sample <= sample + 1;
			serial_bit_valid <= 0;
		end
	end
	else
	begin
		clkdiv <= clkdiv + 1;
		serial_bit_valid <= 0;
	end
end



reg       active;
reg [2:0] serial_bit_pos;
reg       stop_detect;

initial
begin
	active         <= 0;
	serial_bit_pos <= 0;
	stop_detect    <= 0;

	byte_valid     <= 0;
	stop_error     <= 0;
	start_byte     <= 0;
	end_byte       <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		active         <= 0;
		serial_bit_pos <= 0;
		stop_detect    <= 0;

		byte_valid     <= 0;
		stop_error     <= 0;
		start_byte     <= 0;
		end_byte       <= 0;
	end
	else if (serial_bit_valid)
	begin
		if (stop_detect)
		begin
			if (serial_bit != 1'b1)
				stop_error <= 1;

			stop_detect <= 0;
			active      <= 0;

			byte_valid <= serial_bit;
			start_byte <= 0;
			end_byte   <= serial_bit;

		end
		else if (active)
		begin
			byte <= { serial_bit, byte[7:1] };
			serial_bit_pos <= serial_bit_pos + 1;
			stop_detect <= (serial_bit_pos == 7);

			byte_valid <= 0;
			start_byte <= 0;
			end_byte   <= 0;
		end
		else
		begin
			active     <= (serial_bit == 0);
			byte_valid <= 0;
			start_byte <= (serial_bit == 0);
			end_byte   <= 0;
		end
	end
	else
	begin
		byte_valid <= 0;
		start_byte <= 0;
		end_byte   <= 0;
	end
end

endmodule
