module error_serialize(
	clock, reset, enable,
	mask, mask_change, mask_read,
	byte_valid, byte, byte_read);

input wire clock;
input wire reset;
input wire enable;

input wire [31:0] mask;
input wire        mask_change;
output reg        mask_read;

output reg       byte_valid;
output reg [7:0] byte;
input wire       byte_read;

reg [23:0] fifo;
reg [ 1:0] count;

initial
begin
	mask_read  <= 0;
	byte_valid <= 0;
	count      <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		mask_read  <= 0;
		byte_valid <= 0;
		count      <= 0;
	end
	else if (byte_valid && byte_read)
	begin
		byte       <= fifo[7:0];
		fifo[15:0] <= fifo[23:8];
		count      <= count - 1;
		byte_valid <= (count != 0);
		mask_read  <= 0;
	end
	else if (!byte_valid && mask_change && enable)
	begin
		fifo       <= mask[31:8];
		byte       <= mask[ 7:0];
		byte_valid <= 1;
		count      <= 3;
		mask_read  <= 1;
	end
	else
	begin
		mask_read <= 0;
	end
end

endmodule
