module serial_tx(
	clock, reset,
	baud, tx,
	byte, byte_valid,
	active, start_byte, end_byte);

input wire clock;
input wire reset;

input wire [15:0] baud;
output reg        tx;

input wire [7:0] byte;
input wire       byte_valid;

output reg active;

output reg start_byte;
output reg end_byte;


`define MODE_START 0
`define MODE_STOP  (`MODE_START + 8)
`define MODE_END   (`MODE_STOP  + 1)

reg [15:0] clkdiv;
reg        busy;
reg [ 3:0] mode;
reg [ 7:0] tx_byte;

initial
begin
	clkdiv     <= 0;
	active     <= 0;
	start_byte <= 0;
	end_byte   <= 0;
	busy       <= 0;
	tx         <= 1;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		clkdiv     <= 0;
		active     <= 0;
		start_byte <= 0;
		end_byte   <= 0;
		busy       <= 0;
		tx         <= 1;
	end
	else if (!busy)
	begin
		if (byte_valid)
		begin
			mode    <= `MODE_START;
			clkdiv  <= 0;
			busy    <= 1;
			tx      <= 0;
			tx_byte <= byte;
			active  <= 1;
		end
		else
		begin
			active <= 0;
		end
	end
	else if (clkdiv >= baud)
	begin
		clkdiv <= 0;
		if (mode >= `MODE_END)
		begin
			busy <= 0;
		end
		else
		begin
			if (mode == `MODE_STOP)
			begin
				tx <= 1;
			end
			else
			begin
				tx <= tx_byte[0];
				tx_byte[6:0] <= tx_byte[7:1];
			end
			mode <= mode + 1;
		end
	end
	else
	begin
		clkdiv <= clkdiv + 1;
	end

	start_byte <= (!busy && byte_valid);
	end_byte <= (busy && (clkdiv >= baud) && (mode >= `MODE_END));
end

endmodule
