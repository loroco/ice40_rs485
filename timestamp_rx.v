module timestamp_rx(
	clock, reset,
	timer,
	rx_data, rx_data_ready,
	rx_start, rx_end,
	fifo_wr, fifo_data,
	error);

input wire clock;
input wire reset;

input wire [31:0] timer;

input wire [7:0] rx_data;
input wire       rx_data_ready;

input wire rx_start;
input wire rx_end;

output reg       fifo_wr;
output reg [7:0] fifo_data;

output reg error;


reg [ 3:0] mode;
reg [31:0] ts_start;
reg        ts_start_set;
reg [31:0] ts_end;
reg        ts_end_set;
reg [ 7:0] data;
reg        data_set;
reg        write;

initial
begin
	fifo_wr      <= 0;
	error        <= 0;
	mode         <= 0;
	ts_start_set <= 0;
	ts_end_set   <= 0;
	data_set     <= 0;
	write        <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		fifo_wr      <= 0;
		error        <= 0;
		mode         <= 0;
		ts_start_set <= 0;
		ts_end_set   <= 0;
		data_set     <= 0;
		write        <= 0;
	end
	else
	begin
		if ((mode[3] == 1) && write)
		begin
			fifo_wr   <= 1;
			fifo_data <= data;
			mode      <= 0;

			write <= ((ts_start_set | rx_start)
				& (ts_end_set | rx_end)
				& rx_data_ready);

			data_set <= rx_data_ready;
			if (rx_data_ready)
				data <= rx_data;

			if (rx_start)
			begin
				ts_start     <= timer;
				ts_start_set <= 1;
			end

			if (rx_end)
			begin
				ts_end     <= timer;
				ts_end_set <= 1;
			end

			error <= ((rx_start & ts_start_set)
				| (rx_end & ts_end_set));
		end
		else if ((mode[2] == 0) && write)
		begin
			fifo_wr   <= 1;
			fifo_data <= ts_start[7:0];
			mode      <= mode + 1;

			if (rx_data_ready)
			begin
				data     <= rx_data;
				data_set <= 1;
			end

			if (mode[1:0] == 3)
			begin
				if (rx_start) ts_start <= timer;
				ts_start_set <= rx_start;
			end
			else
			begin
				ts_start[23:0] <= ts_start[31:8];
			end

			if (rx_end)
			begin
				ts_end     <= timer;
				ts_end_set <= 1;
			end

			error <= ((rx_data_ready & data_set)
				| (rx_start & (mode[1:0] != 3))
				| (rx_end & ts_end_set));
		end
		else if ((mode[2] == 1) && write)
		begin
			fifo_wr   <= 1;
			fifo_data <= ts_end[7:0];
			mode      <= mode + 1;

			if (rx_data_ready)
			begin
				data     <= rx_data;
				data_set <= 1;
			end

			if (rx_start)
			begin
				ts_start     <= timer;
				ts_start_set <= 1;
			end

			if (mode[1:0] == 3)
			begin
				if (rx_end) ts_end <= timer;
				ts_end_set <= rx_end;
			end
			else
			begin
				ts_end[23:0] <= ts_end[31:8];
			end

			error <= ((rx_data_ready & data_set)
				| (rx_start & ts_start_set)
				| (rx_end & (mode[1:0] != 3)));
		end
		else
		begin
			fifo_wr <= 0;

			if (rx_data_ready)
			begin
				data     <= rx_data;
				data_set <= 1;
			end

			if (rx_start)
			begin
				ts_start     <= timer;
				ts_start_set <= 1;
			end

			if (rx_end)
			begin
				ts_end     <= timer;
				ts_end_set <= 1;
			end

			write <= ((ts_start_set | rx_start)
				& (ts_end_set | rx_end)
				& (data_set | rx_data_ready));

			error <= ((rx_data_ready & data_set)
				| (rx_start & ts_start_set)
				| (rx_end & ts_end_set));
		end
	end
end

endmodule
