module timer(clock, reset, timer);

input  wire        clock;
input  wire        reset;
output reg  [31:0] timer;

`define CLOCK_RATE 12000000
`define RESOLUTION 1000000
`define DIVIDE (`CLOCK_RATE / `RESOLUTION)

reg [15:0] clkdiv;

initial
begin
	timer  <= 0;
	clkdiv <= 0;
end

always @ (posedge clock)
begin
	if (reset)
	begin
		timer  <= 0;
		clkdiv <= 0;
	end
	else if (clkdiv >= (`DIVIDE - 1))
	begin
		clkdiv <= 0;
		timer  <= timer + 1;
	end
	else
	begin
		clkdiv <= clkdiv + 1;
	end
end

endmodule
