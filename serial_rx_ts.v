module serial_rx_ts(
	clock, reset,
	timer,
	baud, fract, rx,
	tx_mask,
	ts_ready, ts_data, ts_read, ts_error,
	ts_fifo_full, ts_fifo_error,
	stop_error);

input wire clock;
input wire reset;

input wire [31:0] timer;

input wire [15:0] baud;
input wire [15:0] fract;
input wire        rx;

input wire tx_mask;

output wire       ts_ready;
output wire [7:0] ts_data;
input  wire       ts_read;
output wire       ts_error;

output wire ts_fifo_full;
output wire ts_fifo_error;
output wire stop_error;

wire [7:0] byte;
wire       byte_valid;
wire       start_byte;
wire       end_byte;
serial_rx rx_uart(
	.clock      (clock),
	.reset      (reset | tx_mask),
	.baud       (baud),
	.fract      (fract),
	.rx         (rx),
	.byte       (byte),
	.byte_valid (byte_valid),
	.stop_error (stop_error),
	.start_byte (start_byte),
	.end_byte   (end_byte));

wire       ts_fifo_wr;
wire [7:0] ts_fifo_data;
timestamp_rx ts(
	.clock         (clock),
	.reset         (reset),
	.timer         (timer),
	.rx_data       (byte),
	.rx_data_ready (byte_valid),
	.rx_start      (start_byte),
	.rx_end        (end_byte),
	.fifo_wr       (ts_fifo_wr),
	.fifo_data     (ts_fifo_data),
	.error         (ts_error));

wire ts_fifo_empty;
byte_fifo ts_fifo(
	.clock   (clock),
	.reset   (reset),
	.wr_en   (ts_fifo_wr),
	.wr_data (ts_fifo_data),
	.rd_en   (ts_read),
	.rd_data (ts_data),
	.empty   (ts_fifo_empty),
	.full    (ts_fifo_full),
	.error   (ts_fifo_error));

assign ts_ready = !ts_fifo_empty;

endmodule
